from django.urls import path, include
from .views import home
from .views import plastico
from .views import agregar
from .views import modificar
from .views import eliminar
from .views import lista
from .views import vidrio
from .views import registrar
from .views import ReciclajeViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('reciclajes', ReciclajeViewSet)

urlpatterns = [
    path('', home,name="home"),
    path('plastico/', plastico,name="plastico"),
    path('vidrio/', vidrio,name="vidrio"),
    path('agregar/', agregar,name="agregar"),
    path('modificar/<id>/',modificar,name="modificar"),
    path('eliminar/<id>/',eliminar,name="eliminar"),
    path('lista/',lista,name="lista"),
    path('registrar/',registrar,name="registrar"),
    path('api/', include(router.urls)),

]