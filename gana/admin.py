from django.contrib import admin
from .models import Tipo, Reciclaje

# Register your models here.

class ReciclaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'fecha', 'cantidad', 'tipo', 'imagen']


admin.site.register(Tipo)
admin.site.register(Reciclaje, ReciclaAdmin)
