from django.shortcuts import render, redirect
from .models import Reciclaje
from .forms import Reciclajeform, CustomUserForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate

from rest_framework import viewsets
from .serializers import ReciclajeSerializer
# Create your views here.

def home(request):  
    return render(request,'gana/home.html')

def vidrio(request):  
    return render(request,'gana/vidrio.html')


def plastico(request):  
    return render(request,'gana/plastico.html')


def lista(request):
    reciclajes = Reciclaje.objects.all()
    data = {
        'reciclajes':reciclajes
    }
    return render(request,'gana/lista.html', data)

@login_required
def agregar(request):
    data = {
        'form':Reciclajeform()
    }
    
    if request.method == 'POST':
        formulario = Reciclajeform(request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Guardado correctamente"
    return render(request,'gana/agregar.html', data)

@login_required
def modificar(request,id):
    reciclaje = Reciclaje.objects.get(id=id)
    data = {
        'form':Reciclajeform(instance=reciclaje)
    }

    if request.method == 'POST':
        formulario = Reciclajeform(data=request.POST, instance=reciclaje, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Modificado correctamente"
            data['form'] = formulario
    return render(request,'gana/modificar.html', data)

@login_required
def eliminar(request,id):
    reciclaje = Reciclaje.objects.get(id=id)
    reciclaje.delete()

    return redirect(to="lista")

def registrar(request):
    data = {
        'form':CustomUserForm()
    }
    if request.method == 'POST':
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()

            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect(to='home')
           
    return render(request,'registration/registrar.html', data)

class ReciclajeViewSet(viewsets.ModelViewSet):
    queryset = Reciclaje.objects.all()
    serializer_class = ReciclajeSerializer