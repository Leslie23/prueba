from django.apps import AppConfig


class GanaConfig(AppConfig):
    name = 'gana'
    verbose_name = "Reciclaje"