from django.db import models

# Create your models here.

class Tipo(models.Model):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre 

class Reciclaje(models.Model):
    nombre = models.CharField(
        max_length=150,
        verbose_name = "Nombre"
        )
    fecha = models.DateField()
    cantidad = models.IntegerField()
    tipo = models.ForeignKey(Tipo, on_delete=models.CASCADE)
    imagen = models.ImageField(null=True, blank=True)
    

    def __str__(self):
        return self.nombre 
