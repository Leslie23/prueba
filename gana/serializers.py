from rest_framework import serializers
from .models import Reciclaje

class ReciclajeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reciclaje
        fields = ['nombre', 'fecha', 'cantidad', 'tipo'] 
        
        
        