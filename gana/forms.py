from django import forms
from django.forms import ModelForm
from .models import Reciclaje
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class Reciclajeform(ModelForm):

    class Meta:
        model = Reciclaje 
        fields = ['nombre', 'fecha', 'cantidad', 'tipo', 'imagen']


class CustomUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2')